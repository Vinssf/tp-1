#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
    // on crée une liste avec les char de la liste de mots, et on indique les séparateurs comme vu dans la doc

    char str[] = "Ho hisse, la saucisse!";
    const char *separators = " ,!";

    char *strToken = strtok(str, separators);
    int mots = 0;
    char supprime;
    char remplace;

    printf("saisissez un mot à remplacer \n");
    scanf("%c", &supprime);
    printf("à remplacer par :\n");
    scanf("%c", &remplace);


    while (strToken != NULL)
    {
        // on incrémente chaque mot de la boucle dans la variable "mots" 
        mots = mots + 1;
        if ( strToken == supprime){
            strToken = remplace;
        }
        printf("%s\n", strToken);
       
        // afficher chaque mot n'est pas demandé, mais c'est plus parlant
        strToken = strtok(NULL, separators);
        
    }
    // on affiche le nombre total de mots
    printf("nombre de mots : %i", mots);
}